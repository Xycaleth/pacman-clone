#ifndef WORLD_H
#define WORLD_H

#include <SFML/Graphics.hpp>
#include <array>
#include <queue>

#include "Actors.h"
#include "GhostAI.h"
#include "Level.h"
#include "MoveDirection.h"
#include "Player.h"
#include "UI.h"

class World
{
public:
    World();

    bool CanMove ( const Coordinate& coordinate, MoveDirection::Type moveDirection ) const;

    void Update ( const sf::Input& input, float dt );
    void Draw ( sf::RenderTarget& renderTarget ) const;

private:
    void ResetActors();
    void ResetActorStates();
    void HandleInput ( const sf::Input& input );
    void CheckCollisions();
    void ProcessEvents();

private:
    enum class WorldEvent
    {
        PLAYER_DIED,
        ATE_POWERDOT
    };

    enum class GameState
    {
        INTRO,
        PLAYING,
        PAUSED,
        DIED,
        GAMEOVER
    };

private:
    UI ui;

    Level level;
    Pacman pacman;
    Player player;

    Ghost blinky;
    Ghost pinky;
    Ghost inky;
    Ghost clyde;

    BlinkyGhostAI blinkyAI;
    PinkyGhostAI pinkyAI;
    InkyGhostAI inkyAI;
    ClydeGhostAI clydeAI;

    std::array<Ghost*, 4> ghosts;
    std::array<IGhostAI*, 4> ghostAIs;
    std::queue<WorldEvent> events;
};

#endif
