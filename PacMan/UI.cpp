#include "UI.h"

#include "Player.h"

UI::UI()
{
    if ( !lifeImage.LoadFromFile ("textures/pacman.png") )
    {
        // Error handle.
    }
}

void UI::Render ( sf::RenderTarget& renderTarget, const Player& player ) const
{
    if ( player.lives > 0 )
    {
        sf::Sprite life (lifeImage);
        for ( int i = 0; i < player.lives; i++ )
        {
            life.SetPosition (80.0f - i * (lifeImage.GetWidth() + 5.0f), 750.0f);
            renderTarget.Draw (life);
        }
    }
}