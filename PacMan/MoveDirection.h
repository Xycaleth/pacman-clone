#ifndef MOVEDIRECTION_H
#define MOVEDIRECTION_H

namespace MoveDirection
{
    enum Type
    {
        NONE,
        LEFT,
        DOWN,
        RIGHT,
        UP
    };
}

#endif
