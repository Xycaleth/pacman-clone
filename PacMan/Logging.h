#ifndef LOGGING_H
#define LOGGING_H

#include <string>

enum LogLevel
{
    LOG_DEBUG,
    LOG_NOTICE,
    LOG_WARNING,
    LOG_ERROR
};

void Log ( LogLevel level, const std::string& logMessage );
void Log ( const std::string& logMessage );

#endif
